/*#control node
resource "aws_instance" "terraform_control" {
  ami                     = var.image
  instance_type           = var.instance
  security_groups         = [aws_security_group.TF_SG.name]
  private_ip              = "172.31.48.114"
  user_data               = file("postinstall.sh")
  tags = {
    Name  = "terraform_control"
    owner = var.owner
  }
}

*/

# Control node
resource "aws_instance" "terraform_control" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  private_ip      = "172.31.48.104"
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "terraform_control"
  }
}

# Worker node 1
resource "aws_instance" "terraform_worker1" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "172.31.48.101"
  #user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "terraform_worker1"
  }
}

# Worker node 2
resource "aws_instance" "terraform_worker2" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "172.31.48.102"
  #user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "terraform_worker2"
  }
}

# Worker node 3
resource "aws_instance" "terraform_worker3" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "10.0.0.103"
  #user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "terraform_worker3"
  }
}
/*
resource "aws_security_group" "TF_SG" {
  name        = "security group using Terraform"
  description = "security group using Terraform"
  vpc_id      = var.vpc

  tags = {
    Name = "TF_SG"
  }
}
*/
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http"
  description = "Allow http inbound traffic"


  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  ingress {
    description = "k3s"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "allow_http_ssh"
  }
}

# Internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}
# VPC
resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
}

